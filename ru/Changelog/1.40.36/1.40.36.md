**Huntbox 1.40.68**

**РЕЛИЗ: 06**/**05**/**2021**

**ГЛАВНОЕ**

**THF Marketplace**

В облачных Huntbox добавлена возможность для быстрого заказа и внедрения модуля THF Polygon Cloud, платформы для динамического анализа и детонации объектов в изолированной среде. Доступна пользователям с ролью "Администратор" с главной страницы системы в случае, если компания еще не имеет используемых модулей типа Polygon.

**События Huntpoint**

Раздел получил ряд структурных улучшений: добавлена панель фильтров со сгруппированными типами событий, переработан верхний уровень таблицы с событиями, реализованы быстрые фильтры, содержимое событий перенесено в боковую панель (сайдбар).

**IR-терминал для Huntpoint**

Новый инструмент для реагирования на угрозы. Интерактивный терминал позволяет аналитику выполнять ручные команды на защищаемых устройствах. Опция доступна только для установок, где явно включена в настройках (по умолчанию везде выключена), только для пользователей с ролями "Администратор" и "Owner" при условии использовании ими двухфакторной аутентификации. Работа в терминале журналируется, а история всех действий прозрачно доступна администраторам компании.

**Раздел** “**Приложения**”

Раздел получил развитие в виде возможности просмотра деталей по всем обнаруженным в организации приложениям: распределение версий, статус установки, список устройств с приложением. Кроме этого, на странице компьютера добавлен новый таб со списком установленного на компьютере ПО, а в разделе с активами можно искать устройства с определенной программой при помощи токена installed_software (например: `installed_software: "Adobe AIR”`)

**Полное представление структуры писем**

Информация о письмах, обработанных системой, теперь содержит максимально полное представление структуры письма, включающее информацию обо всех вложения и ссылках, даже если они по какой-то причине не анализировались в системе (например из-за формата, белого листа или работы другой фильтрующей логики). Это позволяет более корректно осуществлять поиск по письмам и отображать действительную структуру обработанных писем.

**Новая форма загрузки файлов на анализ в Polygon**

Полностью переработана форма загрузки файлов на ручной анализ. Добавились следующие возможности:

- Групповая загрузки файлов (до 5 за раз)

- Загрузка необходимого для анализа контекста (например .eml-файлов с письмами)

- Возможность передачи аргументов командной строки

- Возможность указать произвольный DNS-сервер для использования в системе

- Управление другими опциями анализа (эмуляцией пользовательской активности, настройками сети, параметров статического анализа)

**Другое**

60+ других улучшений и исправлений, включая:

- Перегруппировка меню: корневой раздел My Company для разделов с активами компании (Applications, Assets и другие новые разделы)

- Добавлена возможность поделиться ссылкой на отчет TI&A, улучшения по производительности раздела

- Ряд визуальных и функциональных улучшений в PDF-версии отчёта по файлу

- Добавлен вывод OTP-ключа при включении двухфакторной аутентификации;

- Блок настроек для выявления скрытых каналов в настройках сенсора;

- Оптимизация скорости поиска в разделе События Huntpoint;

- Улучшение виджета "Структура проанализированных писем";

- Оптимизация функциональности генерации отчетов
