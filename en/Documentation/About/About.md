# Group-IB THF Summary

Mar 23, 2021 ・ 3 min read ・ 1203 views

## About us[link](#О-компании)

**Group-IB** is a leading global developer of solutions aimed at detection and prevention of cyberattacks, online fraud and intellectual property protection.

---

Group-IB ecosystem includes:

- **Threat Intelligence & Attribution** is a system designed for analyzing and attributing cyberattacks, proactive threat hunting, and protecting network infrastructure based on data relating to adversary tactics, tools, and activity.
- **Threat Hunting Framework** provides reactive protection and  proactive threat hunting both within and outside the protected perimeter.
- **Fraud Hunting Platform** is designed for fraud prevention in real time and threat actor identification.
- **Digital Risk Protection** provides an AI-driven digital risk identification and mitigation platform.
- **Group-IB competency center** deals with prevention, response, and investigation of cybercrimes.

![](/en/Documentation/About/img/1.png)

Group-IB ecosystem

## General information about Threat Hunting Framework[link](#Общая-информация-о-Threat-Hunting-Framework)

**Group-IB Threat Hunting Framework (THF)** is a solution designed to detect complex targeted attacks and unknown threats, threat hunting both within and outside the protected perimeter, incident response, and investigation.

## Threat Hunting Framework capabilities:

- Protecting corporate email against spear phishing and mass spamming containing malware.
- Protecting the network perimeter, servers and AWS against ransomware, Trojans, worms, viruses, keyloggers and spyware, including those distributed in uncontrolled network flows.
- Protecting infrastructure against adversary surveillance and control.
- Secure file transfer between file storages.
- Malware analysis tool.
- Protecting client's infrastructure using API.
- Protecting workstations and servers against potentially unwanted applications and untrusted devices (Roadmap 2021).
- Remote incident response by CERT-GIB experts and Group-IB Digital Forensics Laboratory.
- Threat hunting in the protected infrastructure.
- Adversary infrastructure detection and analysis.
- Collecting forensic data and recovering the complete attack timeline from the network connection to the infection vector.
- Monitoring of transmitted artifacts in encrypted traffic.
- Monitoring of encrypted traffic in the network.
- Protecting ICS networks against illegitimate data transmission devices.
- Protecting ICS networks against unauthorized PLC modifications.
- Protecting ICS networks against  adversary substitution of technological protocols' funcitions
- Protecting ICS networks against compromise leading to destruction of equipment entailing industrial disasters.

## **Architecture[link](#Архитектура)**

**Threat Hunting Framework includes the following modules:**

- _**Huntbox**_ is a platform for automated analysis, event correlation, threat hunting and identification of adversary actions targeting the client.
- _**Sensor**_ is a module designed to detect network threats through deep analysis of network traffic. The module is used to integrate with the client's IT systems.
- _**Sensor Industrial**_ is a module designed to protect the ICS networks against targeted attacks and ensure the integrity of ACS software by analyzing industrial protocols and comprehensive protection of the corporate network.
- _**Polygon**_ is a malware detonation platform. A module designed for threat detection through behavioral analysis of emails, files, and URLs in an isolated environment.
- _**Huntpoint**_ is a module designed to protect user workstations from threats by presenting a complete timeline of events on the workstation, detecting anomalies, blocking malicious files, isolating the host, and collecting forensic data.
- _**Decryptor**_ is a module designed to decrypt TLS/SSL traffic in the protected infrastructure.

The solution may also include CERT-GIB monitoring service.

- **CERT-GIB** provides event monitoring service, incident detection, malware analysis , incident containment, and recommendations.  
  CERT-GIB is an IMPACT partner, accredited member of FIRST and Trusted Introducer, officially authorized by Carnegie Mellon University and licensed to use the CERT trademark.
